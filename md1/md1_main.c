#include <stdio.h>
#include <stdlib.h>

#include "md1.h"

void printUsage()
{
	printf("Calculate sum 1,2,...,N (limited to 32 bits)\n");
	printf("Usage:	md1 N\n");
}

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		printUsage();
		exit(EXIT_FAILURE);
	}

	int n = atoi(argv[1]);
	printf("%d\n", asum(n));
	return 0;
}
