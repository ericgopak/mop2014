#!/bin/bash

my_var="0 1 2 3 17 123 1234 12345 60000 65535"

for i in $my_var; do
	ret_val=$(qemu-arm -L /usr/arm-linux-gnueabi md1 $i)
	solution=$((i * (i + 1) / 2))
	if [[ $ret_val -eq $solution ]]; then
		echo OK $i #($ret_val == $solution)
	else
		echo "FAIL $i (returned: $ret_val, expected: $solution)"
	fi
done

