#include "md1.h"

	.text
	.align 2
	.global asum
	.type asum, %function

asum:
	mov r1, r0
	add r1, #1
	mul r2, r0, r1
	mov r0, r2
	asr r0, #1
	/*mov r0, r2*/
	bx lr

