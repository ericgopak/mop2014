#include "md1.h"

	.text
	.align 2
	.global asum
	.type asum, %function

asum:
	mov r1, r0	@ r1 = N (a)
	mov r2, r0	@ r2 = N + 1 (b)
	add r2, #1

/*** Russian multiplication ***/
	mov r0, #0

loop:
	cmp r1, #0	@ if a == 0
	beq endofloop	@ leave loop
	
	tst r1, #1	@ if even
	beq even	@ goto even
odd:
	add r0, r0, r2	@ r0 += b
even:

postaction:
	lsr r1, #1	@ a /= 2
	lsl r2, #1	@ b *= 2

	b loop

endofloop:
	lsr r0, #1	@ Divide the product a*b by 2

	bx lr

