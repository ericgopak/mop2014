#include <stdio.h>
#include <stdlib.h>

#include "md2.h"

void printUsage()
{
	printf("Multiply two matrices\n");
	printf("Usage:	"__FILE__" < input.txt > output.txt\n");
}

int main(int argc, char* argv[])
{
	int w1, h1, w2, h2;
	int *m1, *m2, *m3;
	int *p;
	int i, j;

	if (scanf("%d%d", &h1, &w1) != 2)
	{
		printf("Nepareizi ievaddati!\n");
		exit(EXIT_FAILURE);
	}
	m1 = (int*) malloc(w1 * h1 * sizeof(int));
	if (m1 == NULL)
	{
		printf("Neizdevās izdalīt atmiņu!\n");
		exit(EXIT_FAILURE);
	}

	p = m1;
	for (i = 0; i < w1 * h1; i++)
	{
		if (scanf("%d", p++) != 1)
		{
			printf("Nepareizi ievaddati!\n");
			exit(EXIT_FAILURE);
		}
	}

	if (scanf("%d%d", &h2, &w2) != 2)
	{
		printf("Nepareizi ievaddati!\n");
		exit(EXIT_FAILURE);
	}
	m2 = (int*) malloc(w2 * h2 * sizeof(int));

	p = m2;
	for (i = 0; i < w2 * h2; i++)
	{
		if (scanf("%d", p++) != 1)
		{
			printf("Nepareizi ievaddati!\n");
			exit(EXIT_FAILURE);
		}
	}

	m3 = (int*) malloc(h1 * w2 * sizeof(int));

	int ret = matmul(h1, w1, m1, h2, w2, m2, m3);

	if (ret != 0)
	{
		printf("Matricas ir nesavietojamas! w1 != h2");
		exit(EXIT_FAILURE);
	}

	printf("%d %d\n", h1, w2);

	p = m3;
	for (i = 0; i < h1; i++)
	{
		for (j = 0; j < w2; j++)
		{
			printf("%d ", *p++);
		}
		printf("\n");
	}

	return 0;
}
