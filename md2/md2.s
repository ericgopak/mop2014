#include "md2.h"

	.text
	.align 2
	.global matmul
	.type matmul, %function

matmul:
	stmfd sp!, {r4-r11, lr} @ sp -= 9*4

	mov r4, r0		@ h1
	mov r5, r1		@ w1
	mov r6, r2		@ *m1
	mov r7, r3		@ h2 check and reuse
	ldr r8, [sp, #36]	@ w2
	ldr r9, [sp, #40]	@ *m2
	ldr r10, [sp, #44]	@ *m3

/* assert w1 == h2 */
	cmp r5, r7
	bne error

/***** now r7 is reusable *****/
	mul r4, r0, r1	@ h1 * w1
	mul r14, r5, r8	@ h2 * w2

	mov r0, #0	@ a1 (address offset for m1)
	mov r7, #0
	mov r11, #0

test1:
	cmp r0, r4	@ a1 < h1 * w1
	bge end
loop1:
	mov r1, #0	@ j
test2:
	cmp r1, r8	@ j < w2
	bge endloop1
loop2:
	mov r2, r1	@ a2 = j (address offset for m2)
	mov r3, #0	@ sum = 0
test3:
	cmp r2, r14	@ a2 < w2 * h2
	bge endloop2
loop3:
	b body
body:
	@ m3[i][j] += m1[i][k] * m2[k][j];
	@ m[y][x] = m[4*(y*w+x)]

	ldr r7, [r6, r0, LSL #2]	@ m1[i][k] = *(m1 + a1)
	ldr r11, [r9, r2, LSL #2]	@ m2[k][j] = *(m2 + a2)

	mul r7, r11, r7		@ x = m1[i][k] * m2[k][j]
	add r3, r3, r7		@ sum += x

endloop3:
	add r0, r0, #1		@ a1++
	add r2, r2, r8		@ a2 += w2
	b test3
endloop2:
	str r3, [r10]		@ *m3 = sum
	add r10, r10, #4	@ m3++

	sub r0, r0, r5		@ a1 -= w1
	add r1, r1, #1		@ j++
	b test2
endloop1:
	add r0, r0, r5		@ a1 += w1
	b test1

end:
	mov r0, #0
	ldmfd sp!, {r4-r11, lr}
	bx lr

error:
	ldmfd sp!, {r4-r11, lr}
	mov r0, #1
	bx lr

