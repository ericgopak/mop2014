#include "agra.h"
#include <stdio.h>

#if defined ANIMATION
#	define FRAME_WIDTH	190
#	define FRAME_HEIGHT	56
#elif defined COLOR_MIX_TEST
#	define FRAME_WIDTH 92
#	define FRAME_HEIGHT 50
#else
#	define FRAME_WIDTH	40
#	define FRAME_HEIGHT	20
#endif

pixcolor_t frameBuffer[FRAME_HEIGHT][FRAME_WIDTH];


// Kadra bufera sākuma adrese
pixcolor_t * FrameBufferGetAddress()
{
	return (pixcolor_t*)frameBuffer;
}

// Kadra platums
int FrameBufferGetWidth()
{
	return FRAME_WIDTH;
}

// Kadra augstums
int FrameBufferGetHeight()
{
	return FRAME_HEIGHT;
}

static char pixelToChar(const pixcolor_t* pixel)
{
	static const char code[8] = {' ', 'B', 'G', 'C', 'R', 'M', 'Y', '*'};
	
	unsigned int mask = ((pixel->r >> 9) << 2) | ((pixel->g >> 9) << 1) | (pixel->b >> 9);

	return code[mask];
}

// Kadra izvadīšana uz "displeja iekārtas".
int FrameShow()
{
	int i, j;
	for (i = 0; i < FRAME_HEIGHT; i++)
	{
		for (j = 0; j < FRAME_WIDTH; j++)
		{
			printf("%c", pixelToChar(&frameBuffer[i][j]));
		}
		printf("\n");
	}

	return 0;
}

