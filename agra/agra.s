#include "agra.h"

@ sort {a, b} non-descending, use t as temporary register
.macro	order a, b, t
	cmp	\a, \b
	movgt	\t, \a
	movgt	\a, \b
	movgt	\b, \t
.endm

@ sort {a, b, c} non-descending, use t as temporary register
.macro	sort a, b, c, t
	order	\a, \b, \t
	order	\b, \c, \t
	order	\a, \b, \t
.endm

@ put pixel, considering op bits of given color
.macro put_pixel label, address, offset, color, t
	lsr	\t, \color, #30	@ op bits
	cmp	\t, #0
	beq	\label\()put_pixel_copy
	cmp	\t, #1
	beq	\label\()put_pixel_and
	cmp	\t, #2
	beq	\label\()put_pixel_or
	@ else xor
	b	\label\()put_pixel_xor
\label\()put_pixel_copy:
	b	\label\()put_pixel_end
\label\()put_pixel_and:
	ldr	\t, [\address, \offset, LSL #2]
	and	\color, \t
	b	\label\()put_pixel_end
\label\()put_pixel_or:
	ldr	\t, [\address, \offset, LSL #2]
	orr	\color, \t
	b	\label\()put_pixel_end
\label\()put_pixel_xor:
	ldr	\t, [\address, \offset, LSL #2]
	eor	\color, \t
	b	\label\()put_pixel_end
\label\()put_pixel_end:
	str	\color, [\address, \offset, LSL #2]
	@bx	lr
.endm

	.text
	.global	setPixColor, pixel, line, fillBottomFlatTriangle, fillTopFlatTriangle, triangleFill, circle
	.type	setPixColor, %function
	.type	pixel, %function
	.type	line, %function
	.type	triangleFill, %function
	.type	circle, %function
	.type	fillBottomFlatTriangle, %function
	.type	fillTopFlatTriangle, %function

@ property: leaves registers unchanged
traceRegisters:
	stmfd	sp!, {r0-r3, lr}
	stmfd	sp!, {r3-r11}
	stmfd	sp!, {r0-r2} @ shift {r0-r2} to have r0 printed
	ldmfd	sp!, {r1-r3}
	ldr	r0, =format
	bl	printf
	ldmfd	sp!, {r3-r11}
	@add	sp, sp, #36
	ldmfd	sp!, {r0-r3, lr}
	bx	lr
@ ------------------------------------------------------------------------------------------
@ property: leaves registers unchanged
sign:
	@ (r0 < r1) ? -1 : ((r0 > r1) ? +1 : 0)
	cmp	r0, r1
	movlt	r0, #-1
	moveq	r0, #0
	movgt	r0, #1
	bx	lr
@ ------------------------------------------------------------------------------------------
setPixColor:
	ldr	r0, [r0]
	ldr	r1, =currentColor
	str	r0, [r1]
	bx	lr
@ ------------------------------------------------------------------------------------------
@ property: leaves registers unchanged
pixel:
	@ r0 = x
	@ r1 = y
	@ r2 = &color

	stmfd	sp!, {r0-r11, lr}
	
	@ldr	r2, [r2]	@ get color
	
	mov	r9, r0	@ x backup
	mov	r10, r1	@ y backup

	@ get frame dimensions and address
	bl	FrameBufferGetHeight	@ h
	mov	r4, r0
	bl	FrameBufferGetWidth	@ w
	mov	r5, r0
	bl	FrameBufferGetAddress	@ &frame
	mov	r6, r0

	mov	r0, r9
	mov	r1, r10

	@ assert 0 <= x < w
	cmp	r0, #0
	blt	skip_put_pixel
	cmp	r0, r5
	bge	skip_put_pixel
	@ assert 0 <= y < h
	cmp	r1, #0
	blt	skip_put_pixel
	cmp	r1, r4
	bge	skip_put_pixel
	
	@ offset = 4*(y*w+x)
	mla	r1, r5, r1, r0
	ldr	r2, [r2]
	
	put_pixel	"pixel", r6, r1, r2, r0

skip_put_pixel:

	ldmfd	sp!, {r0-r11, pc}

@ ------------------------------------------------------------------------------------------
@ Bresenham's straight line algorithm
line:
	stmfd	sp!, {r4-r11, lr}
	
	mov	r4, r0	@ x1
	mov	r5, r1	@ y1
	mov	r6, r2	@ x2
	mov	r7, r3	@ y2
	
	ldr	r2, =currentColor
	
	@ dx = (x2 - x1) >= 0 ? x2 - x1 : x1 - x2;
	subs	r8, r6, r4
	sublt	r8, r4, r6
	@ dy = (y2 - y1) >= 0 ? y2 - y1 : y1 - y2;
	subs	r9, r7, r5
	sublt	r9, r5, r7
	
	orrs	r10, r8, r9	@ dx == 0 && dy == 0
	moveq	r0, r4
	bleq	pixel
	tst	r10, r10
	beq	line_end
	
	@ s1 = ((x2 - x1) > 0) ? 1 : ((x2 - x1 < 0) ? -1 : 0);
	mov	r0, r6
	mov	r1, r4
	bl	sign
	mov	r10, r0
	@ s2 = ((y2 - y1) > 0) ? 1 : ((y2 - y1 < 0) ? -1 : 0);
	mov	r0, r7
	mov	r1, r5
	bl	sign
	mov	r11, r0
	
	mov	r0, r4	@ x
	mov	r1, r5	@ y
	
	cmp	r8, r9	@ if (dx < dy)
	@ swap(dx, dy)
	movlt	r12, r8
	movlt	r8, r9
	movlt	r9, r12

	@ swap flag
	movlt	r5, #1
	movge	r5, #0
	
	@ D = 2*dy - dx;
	mov	r3, r9, LSL #1
	sub	r3, r3, r8
	
	movs	r4, r8	@ i = dx

line_loop:
	@ while (i >= 0)
	blt	line_loop_end
	
	bl	pixel

	@ while (D >= 0)
	tst	r3, r3
line_D_loop:
	blt	line_D_loop_end
	
	tst	r5, r5
	@ if (swap) x += s1;
	addgt	r0, r0, r10
	@ else y += s2;
	addle	r1, r1, r11
	
	subs	r3, r3, r8, LSL #1	@ D = D - 2*dx;
	b	line_D_loop

line_D_loop_end:
	
	@ D = D + 2*dy;
	add	r3, r3, r9, LSL #1
	tst	r5, r5
	@ if (swap) y += s2;
	addgt	r1, r1, r11
	@ else x += s1;
	addle	r0, r0, r10

	subs	r4, r4, #1	@ i--
	b	line_loop

line_loop_end:
line_end:
	
	ldmfd	sp!, {r4-r11, pc}

@ ------------------------------------------------------------------------------------------
fillBottomFlatTriangle:
	@ proceed only til y2, not below
	
	stmfd	sp!, {r4-r11, lr}
	
	mov	r4, r0	@ x1
	mov	r5, r1	@ y1
	mov	r6,	r2	@ x2
	mov	r7, r3	@ y2
	ldr	r8, [sp, #36]	@ x3
	ldr	r9, [sp, #40]	@ y3
	
	@ special case: y1 = y2 = y3
	cmp	r5, r7
	blne	fillBottomFlatTriangle_general_case
	
fillBottomFlatTriangle_special_case:
	@ draw a line from min(x) to max(x)
	@ sort x1, x2, x3
	sort	r4, r6, r8, r10
	
	mov	r0, r4
	@mov	r1, r1
	mov r2, r8
	@mov	r3, r3
	bl	line
	b	fillBottomFlatTriangle_end
	
fillBottomFlatTriangle_general_case:
	mov	r0, r6
	mov	r1, r4
	bl	sign
	mov	r10, r0	@ s1
	
	mov	r0, r8
	mov	r1, r4
	bl	sign
	mov	r11, r0	@ s2
	
	@ scanlineY, x1, y1, curx1, curx2
	
	sub	r6, r6, r4	@ (x2 - x1)
	sub	r7, r7, r5	@ (y2 - y1)
	sub r8, r8, r4	@ (x3 - x1)
	sub r9, r9, r5	@ (y3 - y1)
	
	mov	r2, r4	@ curx1
	mov	r3, r4	@ curx2
	
	mov	r1, r5	@ scanlineY
	
fillBottomFlatTriangle_loop:
	@ while (scanlineY <= y2)
	sub	r0, r1, r5	@ scanlineY - y1
	cmp	r0, r7
	bgt	fillBottomFlatTriangle_loop_end
	
	@ line(curx1, scanlineY, curx2, scanlineY);
	stmfd	sp!, {r0-r3}
	mov	r0, r2
	@mov	r1, r1
	mov	r2, r3
	mov	r3, r1
	bl	line
	ldmfd	sp!, {r0-r3}
	
	@ scanlineY --> scanlineY + 1 - y1
	sub	r1, r1, r5
	add	r1, r1, #1
	@ curx1 --> curx1 - x1
	sub	r2, r2, r4
	@ curx2 --> curx2 - x1
	sub	r3, r3, r4
	
	@ unused: r0, r4, r5, r12, r14
	mul	r12, r2, r7
	mul	r14, r6, r1
	
	@ if (r12 > r14)
	cmp	r12, r14
	bgt	fillBottomFlatTriangle_loop_case1
	blt	fillBottomFlatTriangle_loop_case2
	b	fillBottomFlatTriangle_loop_next
	
fillBottomFlatTriangle_loop_case1:
@		while (((r2 + r10)*r7 > r6*r1)) r2 += r10;
	add	r12, r2, r10
	mul	r12, r7, r12
	cmp	r12, r14
	addgt	r2, r2, r10
	bgt	fillBottomFlatTriangle_loop_case1
	b	fillBottomFlatTriangle_loop_next
fillBottomFlatTriangle_loop_case2:
@		while (((r2 + r10)*r7 < r6*r1)) r2 += r10;
	add	r12, r2, r10
	mul	r12, r7, r12
	cmp	r12, r14
	addlt	r2, r2, r10
	blt	fillBottomFlatTriangle_loop_case2
	b	fillBottomFlatTriangle_loop_next

fillBottomFlatTriangle_loop_next:
	mul	r12, r3, r9
	mul	r14, r8, r1
	cmp	r12, r14
	bgt	fillBottomFlatTriangle_loop_case3
	blt	fillBottomFlatTriangle_loop_case4
	b	fillBottomFlatTriangle_loop_finalize
	
fillBottomFlatTriangle_loop_case3:
@		while (((r3 + r11)*r9 > r8*r1)) r3 += r11;
	add	r12, r3, r11
	mul	r12, r9, r12
	cmp	r12, r14
	addgt	r3, r11
	bgt	fillBottomFlatTriangle_loop_case3
	b	fillBottomFlatTriangle_loop_finalize

fillBottomFlatTriangle_loop_case4:
@		while (((r3 + r11)*r9 < r8*r1)) r3 += r11;
	add	r12, r3, r11
	mul	r12, r9, r12
	cmp	r12, r14
	addlt	r3, r11
	blt	fillBottomFlatTriangle_loop_case4
	b	fillBottomFlatTriangle_loop_finalize
	
fillBottomFlatTriangle_loop_finalize:
	@ scanlineY --> scanlineY - 1 + y1
	add	r1, r1, r5
	sub	r1, r1, #1
	@ curx1 --> curx1 + x1
	add	r2, r2, r4
	@ curx2 --> curx2 + x1
	add	r3, r3, r4
	
	add	r1, r1, #1	@ scanlineY++
	b	fillBottomFlatTriangle_loop

fillBottomFlatTriangle_loop_end:
fillBottomFlatTriangle_end:
	
	ldmfd	sp!, {r4-r11, pc}
@ ------------------------------------------------------------------------------------------
fillTopFlatTriangle:
	@ proceed only til y2, not above
	
	stmfd	sp!, {r4-r11, lr}
	
	mov	r4, r0	@ x1
	mov	r5, r1	@ y1
	mov	r6,	r2	@ x2
	mov	r7, r3	@ y2
	ldr	r8, [sp, #36]	@ x3
	ldr	r9, [sp, #40]	@ y3

	@ special case: y1 = y2 = y3
	cmp	r7, r9
	blne	fillTopFlatTriangle_general_case
	
fillTopFlatTriangle_special_case:
	@ draw a line from min(x) to max(x)
	@ sort x1, x2, x3
	sort	r4, r6, r8, r10
	
	mov	r0, r4
	@mov	r1, r1
	mov r2, r8
	@mov	r3, r3
	bl	line
	b	fillTopFlatTriangle_end
	
fillTopFlatTriangle_general_case:
	
	mov	r0, r4
	mov	r1, r8
	bl	sign
	mov	r10, r0	@ s1

	mov	r0, r6
	mov	r1, r8
	bl	sign
	mov	r11, r0	@ s2

	sub	r4, r4, r8	@ (x1 - x3)
	sub	r5, r5, r9	@ (y1 - y3)
	sub	r6, r6, r8	@ (x2 - x3)
	sub	r7, r7, r9	@ (y2 - y3)

	mov	r2, r8	@ curx1
	mov	r3, r8	@ curx2

	mov	r1, r9	@ scanlineY

fillTopFlatTriangle_loop:
	@ while (scanlineY >= y2)
	sub	r12, r1, r9	@ scanlineY - y3
	cmp	r12, r7	@ scanlineY - y3 >= y2 - y3
	blt	fillTopFlatTriangle_loop_end
	
	@ line(curx1, scanlineY, curx2, scanlineY);
	stmfd	sp!, {r0-r3}
	mov	r0, r2
	@ mov	r1, r1
	mov	r2, r3
	mov	r3, r1
	bl	line
	ldmfd	sp!, {r0-r3}

	@ scanlineY - 1 - r5 - y3;
	sub	r0, r1, r5
	sub	r0, r0, r9
	sub	r0, r0, #1
	@ scanlineY - 1 - r7 - y3;
	sub	r1, r1, r7
	sub	r1, r1, r9
	sub	r1, r1, #1
	@ curx1 --> curx1 - x1 = curx1 - r4 - x3
	sub	r2, r2, r4
	sub	r2, r2, r8
	@ curx2 --> curx2 - x2 = curx2 - r6 - x3
	sub	r3, r3, r6
	sub	r3, r3, r8

	mul	r12, r2, r5
	mul	r14, r4, r0
	
	@ if (r12 > r14)
	cmp	r12, r14
	bgt	fillTopFlatTriangle_loop_case1
	blt	fillTopFlatTriangle_loop_case2
	b	fillTopFlatTriangle_loop_next
	
fillTopFlatTriangle_loop_case1:
@ while ((curx1 + s1)*r5 > r4*r0) curx1 += s1;
	add	r12, r2, r10
	mul	r12, r5, r12
	cmp	r12, r14
	addgt	r2, r2, r10
	bgt	fillTopFlatTriangle_loop_case1
	b	fillTopFlatTriangle_loop_next
fillTopFlatTriangle_loop_case2:
@ while ((curx1 + s1)*r5 < r4*r0) curx1 += s1;
	add	r12, r2, r10
	mul	r12, r5, r12
	cmp	r12, r14
	addlt	r2, r2, r10
	blt	fillTopFlatTriangle_loop_case2
	b	fillTopFlatTriangle_loop_next

fillTopFlatTriangle_loop_next:
	mul	r12, r3, r7
	mul	r14, r6, r1
	cmp	r12, r14
	bgt	fillTopFlatTriangle_loop_case3
	blt	fillTopFlatTriangle_loop_case4
	b	fillTopFlatTriangle_loop_finalize
	
fillTopFlatTriangle_loop_case3:
@ while ((curx2 + s2)*r7 > r6*r1) curx2 += s2;
	add	r12, r3, r11
	mul	r12, r7, r12
	cmp	r12, r14
	addgt	r3, r11
	bgt	fillTopFlatTriangle_loop_case3
	b	fillTopFlatTriangle_loop_finalize

fillTopFlatTriangle_loop_case4:
@ while ((curx2 + s2)*r7 < r6*r1) curx2 += s2;
	add	r12, r3, r11
	mul	r12, r7, r12
	cmp	r12, r14
	addlt	r3, r11
	blt	fillTopFlatTriangle_loop_case4
	b	fillTopFlatTriangle_loop_finalize
	
fillTopFlatTriangle_loop_finalize:
	@ scanlineY --> scanlineY + 1 + r7 + y3;
	add	r1, r1, r7
	add	r1, r1, r9
	add	r1, r1, #1
	@ curx1 --> curx1 + x1 = curx1 + r4 + x3
	add	r2, r2, r4
	add	r2, r2, r8
	@ curx2 --> curx2 + x2 = curx1 + r6 + x3
	add	r3, r3, r6
	add	r3, r3, r8
	
	sub	r1, r1, #1	@ scanlineY--
	b	fillTopFlatTriangle_loop

fillTopFlatTriangle_loop_end:
fillTopFlatTriangle_end:
	
	ldmfd	sp!, {r4-r11, pc}
@ ------------------------------------------------------------------------------------------
triangleFill:
	stmfd	sp!, {r4-r11, lr}
	
	mov	r4, r0	@ x1
	mov	r5, r1	@ y1
	mov	r6,	r2	@ x2
	mov	r7, r3	@ y2
	ldr	r8, [sp, #36]	@ x3
	ldr	r9, [sp, #40]	@ y3
	
	@ sort y1 <= y2 <= y3
	subs	r0, r5, r7	@ if (y1 > y2) {
	@ swap(x1, x2);
	movgt	r1, r4
	movgt	r4, r6
	movgt	r6, r1
	@ swap(y1, y2);
	movgt	r1, r5
	movgt	r5, r7
	movgt	r7, r1
	
	subs	r0, r7, r9	@ if (y2 > y3) {
	@ swap(x2, x3);
	movgt	r1, r6
	movgt	r6, r8
	movgt	r8, r1
	@ swap(y2, y3);
	movgt	r1, r7
	movgt	r7, r9
	movgt	r9, r1

	subs	r0, r5, r7	@ if (y1 > y2) {
	@ swap(x1, x2);
	movgt	r1, r4
	movgt	r4, r6
	movgt	r6, r1
	@ swap(y1, y2);
	movgt	r1, r5
	movgt	r5, r7
	movgt	r7, r1

	@ move x1,y1,x2,y2 to r0-r3
	stmfd	sp!, {r4-r7}
	ldmfd	sp!, {r0-r3}
	@ push x3, y3
	stmfd	sp!, {r8-r9}

	subs	r10, r7, r9	@ if (y2 == y3)
	bleq	fillBottomFlatTriangle
	tst	r10, r10	@ repeat check
	beq	triangle_done

	subs	r10, r5, r7	@ else if (y1 == y2)
	bleq	fillTopFlatTriangle
	tst	r10, r10	@ repeat check
	beq	triangle_done

	@ general case - split the triangle in a top-flat and bottom-flat one
	@ assuming y1 < y2 < y3

	bl	fillBottomFlatTriangle
	
	@ restore r0-r3
	stmfd	sp!, {r4-r7}
	ldmfd	sp!, {r0-r3}
	
	add	sp, sp, #8
	stmfd	sp!, {r8-r9}

	bl	fillTopFlatTriangle
	
triangle_done:
	add	sp, sp, #8	@ remove x3,y3 from stack
	
	ldmfd	sp!, {r4-r11, pc}

@ ------------------------------------------------------------------------------------------
circle:
	stmfd	sp!, {r4-r11, lr}
	stmfd	sp!, {r0-r2}
	ldmfd	sp!, {r4-r6}
	@ r4 = x0
	@ r5 = y0
	@ r6 = R

	bl	FrameBufferGetAddress
	mov	r11, r0	@ &frame

	bl	FrameBufferGetWidth
	mov	r3, r0	@ w

	mov	r0, r6	@ x
	mov	r1, #0	@ y
	mov	r2, #1	@ radiusError = 1-x
	sub	r2, r2, r0

	ldr	r10, =currentColor
	ldr	r10, [r10]

	@ while x >= y
circle_loop:
	cmp	r0, r1
	blt	circle_loop_end

	@ Draw in all 8 octants
	@ (x, y) --> offset [(y + y0)*w + (x + x0)]
	@ 1. (x + x0, y + y0)
	add	r7, r0, r4
	add	r8, r1, r5
	mla	r9, r8, r3, r7
	put_pixel	"circle1", r11, r9, r10, r14
	@ 2. (y + x0, x + y0)
	add	r7, r1, r4
	add	r8, r0, r5
	mla	r9, r8, r3, r7
	@put_pixel	r11, r9, r10, r14
	put_pixel	"circle2", r11, r9, r10, r14
	@ 3. (-x + x0, y + y0)
	sub	r7, r4, r0
	add	r8, r1, r5
	mla	r9, r8, r3, r7
	put_pixel	"circle3", r11, r9, r10, r14
	@ 4. (-y + x0, x + y0)
	sub	r7, r4, r1
	add	r8, r0, r5
	mla	r9, r8, r3, r7
	put_pixel	"circle4", r11, r9, r10, r14
	@ 5. (-x + x0, -y + y0)
	sub	r7, r4, r0
	sub	r8, r5, r1
	mla	r9, r8, r3, r7
	put_pixel	"circle5", r11, r9, r10, r14
	@ 6. (-y + x0, -x + y0)
	sub	r7, r4, r1
	sub	r8, r5, r0
	mla	r9, r8, r3, r7
	put_pixel	"circle6", r11, r9, r10, r14
	@ 7. (x + x0, -y + y0)
	add	r7, r0, r4
	sub	r8, r5, r1
	mla	r9, r8, r3, r7
	put_pixel	"circle7", r11, r9, r10, r14
	@ 8. (y + x0, -x + y0)
	add	r7, r1, r4
	sub	r8, r5, r0
	mla	r9, r8, r3, r7
	put_pixel	"circle8", r11, r9, r10, r14

	add	r1, r1, #1

	@ if radiusError < 0
	cmp	r2, #0
	bge	circle_re_ge

circle_re_lt:
	@ radiusError += 2y+1
	add	r2, r2, r1, LSL #1
	add	r2, r2, #1

	b	circle_loop

circle_re_ge:
	@ x--
	sub	r0, r0, #1
	@ radiusError += 2(y-x+1)
	sub	r7, r1, r0
	add	r7, r7, #1
	add	r2, r2, r7, LSL #1

	b	circle_loop

circle_loop_end:

	ldmfd	sp!, {r4-r11, pc}


	.data
currentColor:	.word 0x00000000
format:	.asciz "r0=%08x r1=%08x r2=%08x r3=%08x r4=%08x r5=%08x r6=%08x r7=%08x r8=%08x r9=%08x r10=%08x r11=%08x\n"
