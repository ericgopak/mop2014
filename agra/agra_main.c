#include <stdio.h>
#include <stdlib.h>

#include <math.h>

#include "agra.h"

static pixcolor_t empty_pixel = {0x000, 0x000, 0x000, 0}; // ' '
static pixcolor_t white_pixel = {0x3ff, 0x3ff, 0x3ff, 0}; // '*'
static pixcolor_t   red_pixel = {0x3ff, 0x0ff, 0x000, 0}; // 'R'
static pixcolor_t green_pixel = {0x000, 0x3ff, 0x000, 0}; // 'G'
static pixcolor_t  blue_pixel = {0x000, 0x000, 0x3ff, 0}; // 'B'

static void filled_circle(const int cx, const int cy, const int r, pixcolor_t* color)
{
	int i, j;
	int h = FrameBufferGetHeight();
	int w = FrameBufferGetWidth();
	for (i = 0; i < h; i++)
	{
		for (j = 0; j < w; j++)
		{
			if ((cy - i) * (cy - i) + (cx - j) * (cx - j) <= r * r)
			{
				pixel(j, i, color);
			}
		}
	}
}

int main()
{
	pixcolor_t* frame = FrameBufferGetAddress();

	const int h = FrameBufferGetHeight();
	const int w = FrameBufferGetWidth();

	int i, j;

	// notīrīt buferi, aizpildīt katru pikseli ar 0x00000000
	for (i = 0; i < h; i++)
	{
		for (j = 0; j < w; j++)
		{
			((pixcolor_t (*)[w])frame)[i][j] = empty_pixel;
		}
	}

#if defined COLOR_MIX_TEST
	static const pixcolor_t c1 = {0x3ff, 0x000, 0x000, 2};
	static const pixcolor_t c2 = {0x000, 0x3ff, 0x000, 2};
	static const pixcolor_t c3 = {0x000, 0x000, 0x3ff, 2};
	int radius = 35;
	filled_circle(20, 0, radius, &c1);
	filled_circle(w - 1 - 20, 0, radius, &c3);
	filled_circle(w / 2, h - 1, radius, &c2);

#elif defined ANIMATION

	int angle = 0;
	while (1) {
		for (i = 0; i < h; i++) {
			for (j = 0; j < w; j++) {
				pixel(j, i, &empty_pixel);
			}
		}

		setPixColor(&white_pixel);
		line(0, 0, w - 1, 0);
		line(w - 1, 0, w - 1, h - 1);
		line(w - 1, h - 1, 0, h - 1);
		line(0, h - 1, 0, 0);



		double phi = (double)angle / 180 * M_PI;
		int cx = w / 2;
		int cy = h / 2;
		int r = 26;
		
		int x1 = r;
		int y1 = 0;
		int x2 = r * cos( 2 * M_PI / 3) - 0;
		int y2 = r * sin( 2 * M_PI / 3) + 0;
		int x3 = r * cos(-2 * M_PI / 3) - 0;
		int y3 = r * sin(-2 * M_PI / 3) + 0;
		
		// printf("(%d,%d) (%d,%d) (%d,%d)\n", x1, y1, x2, y2, x3, y3);
		printf("angle = %d\n", angle);
		
		triangleFill(
		      round(cx + x1 * cos(phi) - y1 * sin(phi))
			, round(cy + x1 * sin(phi) + y1 * cos(phi))
			, round(cx + x2 * cos(phi) - y2 * sin(phi))
			, round(cy + x2 * sin(phi) + y2 * cos(phi))
			, round(cx + x3 * cos(phi) - y3 * sin(phi))
			, round(cy + x3 * sin(phi) + y3 * cos(phi))
		);
		
		angle = (angle + 10) % 360;
		
		pixel(cx, cy, &green_pixel);
		
		setPixColor(&empty_pixel);
		circle(cx, cy, 5);
		
		// filled_circle(cx, cy, 5, &red_pixel);
		
		setPixColor(&red_pixel);
		
		FrameShow();
		
		char _ = getchar();
	}
	
#else
	
	// border lines
	setPixColor(&white_pixel);
	line(0, 0, w - 1, 0);
	line(w - 1, 0, w - 1, h - 1);
	line(w - 1, h - 1, 0, h - 1);
	line(0, h - 1, 0, 0);
	
	// zīmēt pikseli koordinātās (25,2), baltu.
	pixel(25, 2, &white_pixel);

	// zīmēt līniju no (0,0) līdz (39,19), zilu, ar intensitāti 0x03ff
	setPixColor(&blue_pixel);
	line(0, 0, 39, 19);

	// zīmēt aizpildītu trijstūri, zaļu, ar intensitāti 0x03ff
	setPixColor(&green_pixel);
	triangleFill(38, 10, 5, 17, 28, 4);

	// zīmēt riņķa līniju ar centru (20,10) un rādiusu 5, sarkanu, ar intensitāti 0x03ff
	setPixColor(&red_pixel);
	circle(20, 10, 5);

#endif
	
	// izsaukt funkciju FrameShow()
	FrameShow();
	
	return 0;
}
