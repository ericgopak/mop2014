	.text
	.align	2
	.global	add
	.type	add, %function
add:
	add r0, r0, r1
	bx lr
	.size	add, .-add
