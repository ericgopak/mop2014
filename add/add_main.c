#include <stdio.h>
#include <stdlib.h>

int add(int a, int b);

int main ()
{
	const int a = 11;
	const int b = 12;
	printf("add(%d, %d) = %d\n", a, b, add(a, b));
	return 0;
}
