.text
.global	test
.align	2

test:
	stmfd	sp!, {r4-r7, r10}
	mov	r10, sp

	mov	r1, #1
	mov	r2, #2
	mov	r3, #3
	mov	r4, #4
	mov	r5, #5
	mov	r6, #6
	mov	r7, #7

	str	r1, [sp, #-4]!
	stmfd	sp!, {r2, r3, r4}
	stmfd	sp, {r5, r6}
	str	r7, [sp], #-4
	sub	sp, sp, #8
	ldr	r1, [sp, #4]!
	ldmfd	sp!, {r2, r3, r4}
	ldr	r5, [sp, #-4]
	ldmfd	sp!, {r6}
	ldmfd	sp, {r7}


	@ldr	r0, [sp, #8]
	mov	r0, r1	@ <--- choose register to output

	mov	sp, r10
	ldmfd	sp!, {r4-r7, r10}

	bx	lr

