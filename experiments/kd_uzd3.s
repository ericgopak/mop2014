	.text
	.global simboli
	.align	2

@int simboli(char* str)
simboli:
	@ while(*str != 0)
loop:
	ldrb	r1, [r0]
	cmp	r1, #0
	beq	done
	
	@ if (*str in "eriks")
	cmp	r1, #'e
	beq	varda_burts
	cmp	r1, #'r
	beq	varda_burts
	cmp	r1, #'i
	beq	varda_burts
	cmp	r1, #'k
	beq	varda_burts
	cmp	r1, #'s
	beq	varda_burts
	
	@ else if (*str in "gopaks")
	cmp	r1, #'g
	beq	uzvarda_burts
	cmp	r1, #'o
	beq	uzvarda_burts
	cmp	r1, #'p
	beq	uzvarda_burts
	cmp	r1, #'a
	beq	uzvarda_burts
	cmp	r1, #'k
	beq	uzvarda_burts
	cmp	r1, #'s
	beq	uzvarda_burts

	@ else do not change
	b	post_loop

varda_burts:
	@ *str = '0'
	mov	r2, #'0
	strb	r2, [r0]

	b	post_loop

uzvarda_burts:
	@ *str = '1'
	mov	r2, #'1
	strb	r2, [r0]

	b	post_loop

post_loop:
	@ str++
	add	r0, r0, #1
	b	loop

done:
	mov	r0, #0	@ return 0
	bx	lr

