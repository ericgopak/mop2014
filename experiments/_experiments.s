#include "experiments.h"
	.text
	.align 2
	.global my_func

is_hex:	@(char c)
check_09:
	cmp	r0, #'9
	bgt	check_AF	@ > '9'

	cmp	r0, #'0
	blt	not_hex		@ < '0'

	b	really_hex

check_AF:
	cmp	r0, #'F
	bgt	check_af	@ > 'F'
	
	cmp	r0, #'A
	blt	not_hex		@ < 'A'

	b	really_hex

check_af:
	cmp	r0, #'f
	bgt	not_hex	@ > 'f'
	
	cmp	r0, #'a
	blt	not_hex		@ < 'a'

really_hex:
	mov	r0, #1
	bx	lr

not_hex:
	mov	r0, #0
	bx	lr

my_func:
	stmfd	sp!, {r4, r5, lr}

	mov	r4, r0
	add	r5, r0, r1
	sub	r5, r5, #1
	
	@ldrb	r0, [r5]
	@bl	is_hex
	
test:
	cmp	r4, r5
	bge	done
	
	@while (!is_hex([r4])) r4++;
testa:
	ldrb	r2, [r4]
	bl	is_hex
	bne	testb
	
	add	r4, r4, #1
	b	
testb:
	
	add	r4, r4, #1
	sub	r5, r5, #1
	b	test
	
done:
	ldmfd	sp!, {r4, r5, lr}
	bx	lr

