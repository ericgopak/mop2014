	.data
format:
	.asciz	"%x\n"
my_number:
	.word 0x18C23

	.text
	.global summas
	.align	2

@ eg11041
@   VWXYZ

@int summas()
summas:
	stmfd	sp!, {r4-r11, lr}

	mov	r0, #1	@ for (int n = 1; ; )
	ldr	r3, =0x18C23	@ max n value
loop:
	cmp	r0, r3	@ if (n > #0x18C23)
	bgt	done	@ break
	
	mov	r1, r0
	
	@ r4 = sum  odd_hex_positions(r1)
	@ r5 = sum even_hex_positions(r1)
	mov	r4, #0
	mov	r5, #0
	
	@ max n value is <= 20 bits, i.e. 20 / 4 = 5 shifts is enough
	@ idea: 'catch' last 4 bits and add them to counter
	@ bits [0..3]
	and	r6, r1, #0xF
	add	r4, r4, r6
	@ [4..7]
	lsr	r1, r1, #4
	and	r6, r1, #0xF
	add	r5, r5, r6
	@ [8..11]
	lsr	r1, r1, #4
	and	r6, r1, #0xF
	add	r4, r4, r6
	@ [12..15]
	lsr	r1, r1, #4
	and	r6, r1, #0xF
	add	r5, r5, r6
	@ [16..19]
	lsr	r1, r1, #4
	and	r6, r1, #0xF
	add	r4, r4, r6

	cmp	r4, r5
	beq	print_it

post_loop:
	add	r0, r0, #1
	b	loop

print_it:
	@ printf(format, n)
	stmfd	sp!, {r0-r3}
	mov	r1, r0
	ldr	r0, =format
	bl	printf
	ldmfd	sp!, {r0-r3}
	b	post_loop

done:
	mov	r0, #0	@ return 0
	ldmfd	sp!, {r4-r11, lr}
	bx	lr

