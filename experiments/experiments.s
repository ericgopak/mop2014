	.text
	.align 2
	.global my_fun
my_func:
	stmfd	sp!, {r4-r11}

	ldr	x, =format
	mov	r1, #1
	mov	r2, #2
	mov	r3, #3
	bl	printf

	ldmfd	sp!, {r4-r11}
	bx	lr
.data
format:
.asciz	"%d %d %d\n"
