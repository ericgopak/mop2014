#include <stdio.h>
#include <stdlib.h>

#include "kd1u3.h"

void printUsage()
{
	printf("Replace characters in str: if in 'alpha' then substitude with \"0\", else if in 'beta' then substitude with \"1\"\n");
	printf("Usage:	"__FILE__" base_string alpha_string beta_string\n");
}

int main(int argc, char* argv[])
{
	// Assuming that passed arguments are situated in writable memory
	if (argc - 1 != 3)
	{
		printf("Incorrect number of arguments! Expected %d, but received %d\n", 3, argc - 1);
		printUsage();
		exit(EXIT_FAILURE);
	}

	int result = replace(argv[1], argv[2], argv[3]);
	
	printf("%s\n", argv[1]);
	return 0;
}
