#include "kd1u3.h"
	.text
	.global	replace
	.type	replace, %function
	.align	2

@trace:
@	stmfd	sp!, {r0-r12}
@ ...

replace:
	@ r0 = str
	@ r1 = alpha
	@ r2 = beta

	stmfd	sp!, {r4-r12, lr}

	@ while(*str != 0)
loop:
	ldrb	r3, [r0], #1
	cmp	r3, #0
	beq	done

check_alpha:
	mov	r4, r1	@ r4 = alpha
alpha_loop:
	@ if (*str in alpha)
	ldrb	r5, [r4], #1	@ r5 = *alpha++
	cmp	r5, #0
	beq	check_beta

	cmp	r3, r5
	beq	replace_with_0

	b	alpha_loop

check_beta:
	mov	r6, r2
beta_loop:
	@ else if (*str in beta)
	ldrb	r7, [r6], #1	@ r7 = *beta++
	cmp	r7, #0
	beq	loop

	cmp	r3, r7
	beq	replace_with_1

	b	beta_loop

	@ else do not change
	b	loop

replace_with_0:
	@ *str = '0'
	mov	r6, #'0
	strb	r6, [r0, #-1]

	b	loop

replace_with_1:
	@ *str = '1'
	mov	r6, #'1
	strb	r6, [r0, #-1]

	b	loop

done:
	mov	r0, #0	@ return 0
	ldmfd	sp!, {r4-r12, pc}

	.data
format: .asciz "%d\n"

