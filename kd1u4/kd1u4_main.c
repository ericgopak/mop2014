#include <stdio.h>
#include <stdlib.h>
#include "kd1u4.h"

void printUsage()
{
	printf("Print all 'special' integers from 1 to N\n");
	printf("Usage:	" __FILE__ " N\n");
}

int main(int argc, char* argv[])
{
	if (argc - 1 != 1)
	{
		printf("Incorrect number of arguments! Expected %d, received %d\n", argc - 1, 1);
		exit(EXIT_FAILURE);
	}

	int N = atoi(argv[1]); // TODO: add validation

	int result = list_special(N);

	if (result == 1)
	{
		exit(EXIT_FAILURE);
	}

	return 0;
}
