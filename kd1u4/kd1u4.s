	.data
format: .asciz	"%x\n"

	.text
	.global list_special
	.type	list_special, %function
	.align	2

list_special:
	stmfd	sp!, {r4-r11, lr}

	mov	r1, #1	@ for (int n = 1; n <= N; n++)
loop:
	@ Assuming N < 2^32 - 1 (no overflow occurs)
	cmp	r1, r0	@ if (n > N)
	bgt	done	@ break

	mov	r2, r1
	
	@ r4 = sum  odd_hex_positions(r1)
	@ r5 = sum even_hex_positions(r1)
	mov	r4, #0
	mov	r5, #0
	
	@ max N value is <= 20 bits, i.e. 20 / 4 = 5 shifts is enough
	@ idea: 'catch' last 4 bits and add them to counter
	@ bits [0..3]
	and	r6, r2, #0xF
	add	r4, r4, r6
	@ [4..7]
	lsr	r2, r2, #4
	and	r6, r2, #0xF
	add	r5, r5, r6
	@ [8..11]
	lsr	r2, r2, #4
	and	r6, r2, #0xF
	add	r4, r4, r6
	@ [12..15]
	lsr	r2, r2, #4
	and	r6, r2, #0xF
	add	r5, r5, r6
	@ [16..19]
	lsr	r2, r2, #4
	and	r6, r2, #0xF
	add	r4, r4, r6

	cmp	r4, r5
	beq	print_it

post_loop:
	add	r1, r1, #1
	b	loop

print_it:
	@ printf(format, n)
	stmfd	sp!, {r0-r3}
	ldr	r0, =format
	@ r1 = n
	bl	printf
	ldmfd	sp!, {r0-r3}
	b	post_loop

done:
	mov	r0, #0	@ return 0
	ldmfd	sp!, {r4-r11, pc}

