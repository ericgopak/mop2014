	.text
	.align	2
	.global	max
	.type	max, %function
max:
	cmp	r0, r1
	bge	skip
	mov	r0, r1
skip:
	bx lr
	.size	max, .-max
