#include <stdio.h>
#include <stdlib.h>

int max(int a, int b);

int main ()
{
	const int a = 11;
	const int b = 42; 
	printf("max(%d, %d) = %d\n", a, b, max(a, b));
	return 0;
}
