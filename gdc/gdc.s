	.text
	.align	2
	.global	gdc
	.type	gdc, %function
gdc:
	cmp	r0, r1
	beq	found		@ r0 == r1 == gdc
	bge	r0_greater_then_r1
	sub	r1, r1, r0
	b	gdc
r0_greater_then_r1:
	sub	r0, r0, r1
	b	gdc
	
found:
	bx lr
	.size	gdc, .-gdc

