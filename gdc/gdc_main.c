#include <stdio.h>
#include <stdlib.h>

int gdc(int a, int b);

int main ()
{
	const int a = 126;
	const int b = 36;	
	printf("gdc(%d, %d) = %d\n", a, b, gdc(a, b));
	return 0;
}
